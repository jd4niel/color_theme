# -*- encoding: utf-8 -*-
##################################################################################################
#
#   Author: Experts SAS (www.exdoo.mx)
#   Coded by: Daniel Acosta (daniel.acosta@exdoo.mx)
#   License: https://blog.exdoo.mx/licencia-de-uso-de-software/
#
##################################################################################################
{
    "name": "Color Theme RED!",
    "version": "1.0",
    "depends": ["base",],
    "author": "exdoo.mx",
    "category": "Color Modules",
    "website" : "https://www.exdoo.mx/",
    "description": """ 
        Este módulo pone color rojo
        
        Si tiene dudas, quiere reportar algún error o mejora póngase en contacto con nosotros: info@experts.com.mx
        """,
    "data" : [
        'views/assets.xml',
        ],
    "demo": [],
    "images": [],
    "test": [],
    "installable": True,
    "active": False,
    "certificate": False,
}
